package ch.hockdudu.minesweeper;

import java.util.Arrays;

class Cell {
    boolean isBomb;
    boolean isShown;
    boolean isMarked;
    Cell[] neighbors;

    int bombsNearby() {
        return (int) Arrays.stream(neighbors).filter(cell -> cell.isBomb).count();
    }
}
