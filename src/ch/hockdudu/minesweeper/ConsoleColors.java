package ch.hockdudu.minesweeper;

// https://misc.flogisoft.com/bash/tip_colors_and_formatting
public enum ConsoleColors {

    neighbors0("\033[39m"),         // Default
    neighbors1("\033[1;34m"),       // Bold blue
    neighbors2("\033[1;32m"),       // Bold green
    neighbors3("\033[1;31m"),       // Bold red
    neighbors4("\033[1;95m"),       // Bold magenta
    neighborsMore("\033[0;39m"),    // Bold default
    marked("\033[1;4;33m"),         // Bold underlined yellow
    bomb("\033[5;1;31m"),           // Blink bold red
    hidden("\033[2;39m");           // Dim default

    private String colorCode;
    ConsoleColors(String colorCode) {
        this.colorCode = colorCode;
    }

    String value(int i) {
        return value(String.valueOf(i));
    }

    String value(String s) {
        return colorCode + s + reset;
    }

    private static final String reset = "\033[0m";  // Text Reset
}
