package ch.hockdudu.minesweeper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.stream.Stream;

class Field {

    Cell[][] cells;
    int width;
    int height;

    Field(int width, int height, int numberOfBombs) {

        if (width <= 0 || height <= 0) {
            throw new IllegalArgumentException("Neither width nor height may be smaller or equal to");
        }

        if (width * height < numberOfBombs) {
            throw new IllegalArgumentException("numberOfBombs might not be bigger than width * height");
        }

        cells = new Cell[width][height];
        this.width = width;
        this.height = height;

        // Initializes cells
        for (var i = 0; i < width; i++) {
            for (var j = 0; j < height; j++) {
                cells[i][j] = new Cell();
            }
        }

        // Adds random bombs
        (new Random()).ints(0, width * height).distinct().limit(numberOfBombs)
                .forEach((randomNumber) -> cells[randomNumber / height][randomNumber % height].isBomb = true);

        // Adds neighbors
        for (var x = 0; x < width; x++) {
            for (var y = 0; y < height; y++) {
                var neighbors = new ArrayList<Cell>();

                // Loops neighbors, where N: Neighbor, C: Cell
                // N N N
                // N C N
                // N N N
                for (var neighborX = x - 1; neighborX <= x + 1; neighborX++) {
                    for (var neighborY = y - 1; neighborY <= y + 1; neighborY++) {
                        // Don't add self
                        if (neighborX == x && neighborY == y) continue;

                        try {
                            neighbors.add(cells[neighborX][neighborY]);
                        } catch (IndexOutOfBoundsException ignored) {
                        }
                    }
                }

                cells[x][y].neighbors = neighbors.toArray(new Cell[0]);
            }
        }
    }

    void mark(int x, int y) {
        var cellToMark = cells[x][y];
        if (!cellToMark.isShown) cellToMark.isMarked = !cellToMark.isMarked;
    }

    void open(int x, int y) {
        var cellToOpen = cells[x][y];
        cellToOpen.isShown = true;
        cellToOpen.isMarked = false;

        if (cellToOpen.isBomb) return;

        recursiveOpen(cellToOpen);
    }

    private void recursiveOpen(Cell cell) {
        cell.isShown = true;
        if (cell.bombsNearby() == 0) {
            for (Cell neighbor : cell.neighbors) {
                if (!neighbor.isShown)
                    recursiveOpen(neighbor);
            }
        }
    }

    private Stream<Cell> cellsFlatStream() {
        return Arrays.stream(cells).flatMap(Arrays::stream);
    }

    GameState getGameState() {
        var isBombOpened = cellsFlatStream().anyMatch(cell -> cell.isShown && cell.isBomb);
        if (isBombOpened) return GameState.LOST;

        var areAllCellsOpened = cellsFlatStream().allMatch(cell -> cell.isShown || cell.isBomb && cell.isMarked);
        if (areAllCellsOpened) return GameState.WON;

        return GameState.RUNNING;
    }

    enum GameState {
        RUNNING,
        WON,
        LOST
    }
}
