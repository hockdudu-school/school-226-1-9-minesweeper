package ch.hockdudu.minesweeper;

import java.util.Scanner;
import java.util.regex.Pattern;

class UserInput {
    int x;
    int y;
    Action action;

    enum Action {
        OPEN,
        MARK
    }

    @SuppressWarnings("SameParameterValue")
    static UserInput readUserInput(int maxX, int maxY) {
        var isUserInputValid = false;
        var scanner = new Scanner(System.in);
        var userInputPattern = Pattern.compile("(?<input>[MmOo]) (?<x>\\d+) (?<y>\\d+)");

        var returnUserInput = new UserInput();

        while (!isUserInputValid) {
            var line = scanner.nextLine();
            var matcher = userInputPattern.matcher(line);

            if (!matcher.matches()) continue;

            var parsedInputX = Integer.parseInt(matcher.group("x"));
            var parsedInputY = Integer.parseInt(matcher.group("y"));

            if ((0 < parsedInputX && parsedInputX <= maxX) && (0 < parsedInputY && parsedInputY <= maxY)) {
                isUserInputValid = true;
                parsedInputX--;
                parsedInputY--;
            } else continue;

            switch (matcher.group("input")) {
                case "m":
                case "M":
                    returnUserInput.action = Action.MARK;
                    break;
                case "o":
                case "O":
                    returnUserInput.action = Action.OPEN;
                    break;
            }

            returnUserInput.x = parsedInputX;
            returnUserInput.y = parsedInputY;
        }

        return returnUserInput;
    }
}
