package ch.hockdudu.minesweeper;

class Renderer {
    static void renderField(Field field) {
        var outputStringBuilder = new StringBuilder();

        outputStringBuilder.append("/ ");
        for (var i = 0; i < field.width; i++) {
            outputStringBuilder.append(String.format("%d ", i + 1));
        }
        outputStringBuilder.append('\n');

        for (var y = 0; y < field.height; y++) {
            outputStringBuilder.append(String.format("%d ", y + 1));

            for (var x = 0; x < field.width; x++) {
                outputStringBuilder.append(String.format("%s ", cellToString(field.cells[x][y])));
            }

            outputStringBuilder.append('\n');
        }

        System.out.println(outputStringBuilder);

    }

    private static String cellToString(Cell cell) {
        if (!cell.isShown && !cell.isMarked) return ConsoleColors.hidden.value(".");

        if (cell.isMarked) return ConsoleColors.marked.value("H");

        if (cell.isBomb) return ConsoleColors.bomb.value("X");

        switch (cell.bombsNearby()) {
            case 0:
                return ConsoleColors.neighbors0.value(cell.bombsNearby());
            case 1:
                return ConsoleColors.neighbors1.value(cell.bombsNearby());
            case 2:
                return ConsoleColors.neighbors2.value(cell.bombsNearby());
            case 3:
                return ConsoleColors.neighbors3.value(cell.bombsNearby());
            case 4:
                return ConsoleColors.neighbors4.value(cell.bombsNearby());
            default:
                return ConsoleColors.neighborsMore.value(cell.bombsNearby());
        }
    }
}
