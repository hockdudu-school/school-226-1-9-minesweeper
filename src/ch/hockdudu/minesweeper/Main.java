package ch.hockdudu.minesweeper;

public class Main {

    private static final int width = 9;
    private static final int height = 9;
    private static final int numberOfBombs = 10;

    public static void main(String[] args) {

        Field field = new Field(width, height, numberOfBombs);

        while (field.getGameState() == Field.GameState.RUNNING) {
            Renderer.renderField(field);

            var userInput = UserInput.readUserInput(width, height);

            switch(userInput.action) {
                case MARK:
                    field.mark(userInput.x, userInput.y);
                    break;
                case OPEN:
                    field.open(userInput.x, userInput.y);
                    break;
            }
        }

        Renderer.renderField(field);

        if (field.getGameState() == Field.GameState.WON) {
            System.out.println("Congratulations, you won!");
        } else {
            System.out.println("You lost, better luck next time!");
        }
    }
}
