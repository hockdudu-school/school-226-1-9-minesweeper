package ch.hockdudu.minesweeper;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class FieldTest {

    @Test
    void construct() {
        var testWidth = 15;
        var testHeight = 10;
        var testBombs = 8;
        var field = new Field(testWidth, testHeight, testBombs);

        assertEquals(testWidth, field.width);
        assertEquals(testWidth, field.cells.length);

        assertEquals(testHeight, field.height);
        assertEquals(testHeight, field.cells[0].length);
        assertEquals(testHeight, field.cells[testWidth - 1].length);

        assertEquals(testBombs, Arrays.stream(field.cells).flatMap(Arrays::stream).filter(cell -> cell.isBomb).count());
    }

    @Test
    void mark() {
        var field = new Field(5, 5, 0);

        assertFalse(field.cells[2][3].isMarked);

        field.mark(2, 3);
        assertTrue(field.cells[2][3].isMarked);

        field.mark(2, 3);
        assertFalse(field.cells[2][3].isMarked);


        field.mark(2, 3);
        field.open(2, 3);
        assertFalse(field.cells[2][3].isMarked); // Shouldn't be marked anymore if was opened
    }

    @Test
    void open() {
        var field = new Field(5, 5, 0);

        field.open(2, 3);
        assertTrue(field.cells[2][3].isShown);

        field.open(2, 3);
        assertTrue(field.cells[2][3].isShown); // You shouldn't be able to close
    }

    @Test
    void autoOpen() {
        var field = new Field(5, 5, 0);

        // . x . . .
        // . . . . .
        // . . X . .
        // . . . . .
        // X . . X .
        field.cells[1][0].isBomb = true;
        field.cells[2][2].isBomb = true;
        field.cells[0][4].isBomb = true;
        field.cells[4][3].isBomb = true;

        // . x . . .
        // . . . . .
        // . . X . ?
        // . . . . 1
        // X . . X .
        field.open(3, 4);
        assertFalse(field.cells[2][4].isShown);

        // . x 1?0 0 <-
        // . .?1 1 0
        // . . X 1 0?
        // . . . . 1
        // X . . X .
        field.open(4, 0);
        assertTrue(field.cells[2][0].isShown);
        assertFalse(field.cells[1][1].isShown);
        assertTrue(field.cells[4][2].isShown);
    }

    @Test
    void gameStates() {
        var field = new Field(5, 5, 0);

        assertEquals(Field.GameState.RUNNING, field.getGameState());

        field.open(2, 3); // All cells should open
        assertEquals(Field.GameState.WON, field.getGameState());

        field = new Field(5, 5, 25);
        field.open(2, 3);
        assertEquals(Field.GameState.LOST, field.getGameState());
    }

    @Test
    void invalidData() {
        assertDoesNotThrow(() -> new Field(5, 5, 25));
        assertThrows(IllegalArgumentException.class, () -> new Field(5, 5, 27));
        assertThrows(IllegalArgumentException.class, () -> new Field(0, 0, 0));
        assertThrows(IllegalArgumentException.class, () -> new Field(-5, -5, 0));
        assertThrows(IllegalArgumentException.class, () -> new Field(5, -5, 2));
    }

    @Test
    void neighbors() {
        var field = new Field(5, 5, 0);
        var cells = field.cells;

        // / 0 1 2 3 4
        // 0 . ? . . .
        // 1 . . ? . .
        // 2 . . X?? .
        // 3 . ? . . .
        // 4 . . . . .
        var neighbors = cells[2][2].neighbors;
        var neighborsList = Arrays.asList(neighbors);
        assertFalse(neighborsList.contains(cells[1][0]));
        assertTrue(neighborsList.contains(cells[2][1]));
        assertFalse(neighborsList.contains(cells[2][2])); // Not self
        assertTrue(neighborsList.contains(cells[3][2]));
        assertTrue(neighborsList.contains(cells[1][3]));
        assertEquals(8, neighbors.length);

        // / 0 1 2 3 4
        // 0 . . . . .
        // 1 ? . . . .
        // 2 . . . . .
        // 3 . . . . .
        // 4 . . . . ?
        assertEquals(3, cells[4][4].neighbors.length);
        assertEquals(5, cells[0][1].neighbors.length);
    }

}